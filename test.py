import time

import openai
from g4f.client import Client


def generate_openai_answer(task):
    try:
        with openai.OpenAI(api_key=task) as client:
            completion = client.chat.completions.create(
                model='gpt-4o',
                messages=[{"role": "user",
                           "content": f"Пожалуйста, предоставь максимально подробные сведения. Отвечай будто ты википедия, не в формате диалога а чисто как определение и ответ на то как влияет на организм. Ответ должен быть большим и на русском языке. Вопрос: Влияние на организм {task}"}]
            )
        answer = completion.choices[0].message.content
        return answer
    except Exception as e:
        print('Error', e)
        time.sleep(3)
        return generate_openai_answer(task)


