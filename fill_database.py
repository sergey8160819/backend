import dotenv
import openai
from freeGPT import AsyncClient, Client
from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, Float, Text
from sqlalchemy.ext.declarative import declarative_base
import os
import pandas as pd

from sqlalchemy.orm import relationship, sessionmaker
import time

from g4f.client import Client

dotenv.load_dotenv()

def generate_answer(task):

    try:
        client = Client()
        response = client.chat.completions.create(
            messages=[{"role": "user",
                       "content": f"Пожалуйста, предоставь максимально подробные сведения. Отвечай будто ты википедия, не в формате диалога а чисто как определение и ответ на то как влияет на организм. Ответ должен быть большим и на русском языке. Вопрос: Влияние на организм {task}"}],

        )
        return response.choices[0].message.content
    except Exception as e:
        print('err', e)
        time.sleep(3)
        return generate_answer(task)


def generate_openai_answer(task):

    try:
        with openai.OpenAI(api_key=os.getenv('TOKEN')) as client:
            completion = client.chat.completions.create(
                model='gpt-3.5-turbo',
                messages=[{"role": "user",
                           "content": f"Пожалуйста, предоставь максимально подробные сведения. Отвечай будто ты википедия, не в формате диалога а чисто как определение и ответ на то как влияет на организм. Ответ должен быть большим и на русском языке. Вопрос: Влияние на организм {task}"}]
            )
        answer = completion.choices[0].message.content
        return answer
    except Exception as e:
        print('Error', e)
        time.sleep(3)
        return generate_openai_answer(task)


DB_NAME = os.getenv("DB_NAME")
DB_HOST = os.getenv("DB_HOST")
DB_LOGIN = os.getenv("DB_LOGIN")
DB_PASSWORD = os.getenv("DB_PASSWORD")

db_url = f"mysql://{DB_LOGIN}:{DB_PASSWORD}@{DB_HOST}/{DB_NAME}"
engine = create_engine(db_url)
Base = declarative_base()


class Polymorphism(Base):
    __tablename__ = 'polymorphism'

    id = Column(Integer, primary_key=True)
    name = Column(Text)
    description = Column(Text)
    value = Column(Float)
    color = Column(Text)


class Edges(Base):
    __tablename__ = 'edges'

    id = Column(Integer, primary_key=True)
    polymorphism_id = Column(Integer, ForeignKey('polymorphism.id'))
    group_id = Column(Integer, ForeignKey('group.id'))

    polymorphism = relationship("Polymorphism")
    group = relationship("Group")


class Group(Base):
    __tablename__ = 'group'

    id = Column(Integer, primary_key=True)
    title = Column(Text)
    image = Column(Text)
    description = Column(Text)


Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
session = Session()


def calculate_color(data, value):
    min_value = min(data.values[:, 2])
    max_value = max(data.values[:, 2])
    normalized_value = (value - min_value) / (max_value - min_value)
    red = 255 * normalized_value
    green = 255 * (1 - normalized_value)
    blue = 0
    return f'#{int(red):02x}{int(green):02x}{int(blue):02x}'


def load_data():
    file_id = 'uploads/dataset.xlsx'
    data = pd.read_excel(file_id, header=None)

    nodes = []
    edges = []
    formatted_nodes = []
    for i, row in enumerate(data.values):
        source = f'{str(row[0])} {str(row[1])}'
        color_value = row[2]
        target = str(row[3])

        if source not in nodes:
            nodes.append(source)
            formatted_nodes.append(
                {"id": len(nodes), "label": source, "color": calculate_color(data, color_value), 'value': color_value,
                 "is_group": False})

        if target not in nodes:
            nodes.append(target)
            formatted_nodes.append(
                {"id": len(nodes), "label": target, 'is_group': True})
        edges.append({"from": nodes.index(source) + 1, "to": nodes.index(target) + 1, "color": "white"})
    return formatted_nodes, edges


nodes, edges = load_data()

for index, node in enumerate(nodes):
    if node['is_group']:
        existing_group = session.query(Group).filter_by(title=node['label']).first()
        if not existing_group:
            new_group = Group(
                title=node['label']
            )
            session.add(new_group)
    else:
        existing_polymorphism = session.query(Polymorphism).filter_by(name=node['label']).first()
        if not existing_polymorphism:
            print(node['label'])
            gpt_answer = generate_openai_answer(node['label'])
            new_p = Polymorphism(
                name=node['label'],
                description=gpt_answer,
                value=node['value'],
                color=node['color']
            )
            session.add(new_p)
            session.commit()
    print(index, '/', len(nodes))

session.close()
