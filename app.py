import asyncio
import os
import random
import uuid

import openai
import pandas as pd
from aiohttp import web
from aiohttp.web_middlewares import middleware
import dotenv
from sqlalchemy import create_engine, Column, Integer, Text, Float
from sqlalchemy.orm import declarative_base, sessionmaker

dotenv.load_dotenv()

dotenv.load_dotenv()
DB_NAME = os.getenv("DB_NAME")
DB_HOST = os.getenv("DB_HOST")
DB_LOGIN = os.getenv("DB_LOGIN")
DB_PASSWORD = os.getenv("DB_PASSWORD")

db_url = f"mysql://{DB_LOGIN}:{DB_PASSWORD}@{DB_HOST}/{DB_NAME}"
engine = create_engine(db_url)
Base = declarative_base()

Session = sessionmaker(bind=engine)



class Polymorphism(Base):
    __tablename__ = 'polymorphism'

    id = Column(Integer, primary_key=True)
    name = Column(Text)
    description = Column(Text)
    value = Column(Float)
    color = Column(Text)


@middleware
async def cors_middleware(request, handler):
    response = await handler(request)
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, DELETE'
    response.headers['Access-Control-Allow-Headers'] = 'Content-Type, Authorization'
    return response


def get_description_by_name(name):
    session = Session()
    polymorphism = session.query(Polymorphism).filter_by(name=name).first()
    session.close()
    if polymorphism:
        return polymorphism.description
    else:
        return "Описание отсутствует"


app = web.Application(middlewares=[cors_middleware])


async def generate_handler(request):
    key = os.getenv('TOKEN')
    text = request.query.get('text')
    if text.isdigit():
        description_data = pd.read_excel('uploads/description.xlsx')
        return web.json_response({'text': get_description(description_data, text)})

    # async with openai.AsyncOpenAI(api_key=key) as client:
    #     completion = await client.chat.completions.create(
    #         model='gpt-3.5-turbo-16k',
    #         messages=[{'role': 'system',
    #                    'content': 'Пожалуйста, предоставьте максимально подробные сведения о генетическом полиморфизме с идентификатором  и генотипом которые тебе пишут. Желательно, чтобы ответ включал информацию о местоположении этого полиморфизма в геноме человека, его возможных клинических или фенотипических последствиях, связанных заболеваниях или фенотипических особенностях, а также любую другую значимую информацию, которая может быть связана с этим SNP. Отвечай максимально подробно."'},
    #                   {'role': 'user', 'content': text}],
    #     )
    # answer = completion.choices[0].message.content
    # print(answer)
    return web.json_response({'text': get_description_by_name(text)})


async def menu_handler(request):
    data = pd.read_excel('uploads/menu.xlsx', header=None)
    resp = []
    for d in data.values:
        resp.append({'title': d[0], 'text': d[1]})
    return web.json_response(resp)


async def upload_handler(request):
    data = await request.post()
    file = data['file_links']
    filename = f'uploads/dataset.xlsx'
    content = file.file.read()
    with open(filename, 'wb') as f:
        f.write(content)
    f.close()

    file = data['file_labels']
    filename = f'uploads/description.xlsx'
    content = file.file.read()
    with open(filename, 'wb') as f:
        f.write(content)
    f.close()

    file = data['menu']
    filename = f'uploads/menu.xlsx'
    content = file.file.read()
    with open(filename, 'wb') as f:
        f.write(content)
    f.close()

    return web.json_response({'file_id': filename}, status=200)


def calculate_color(data, value):
    min_value = min(data.values[:, 2])
    max_value = max(data.values[:, 2])
    normalized_value = (value - min_value) / (max_value - min_value)
    red = 255 * normalized_value
    green = 255 * (1 - normalized_value)
    blue = 0
    return f'#{int(red):02x}{int(green):02x}{int(blue):02x}'


def get_description(description, id):
    for d in description.values:
        if str(d[0]) == str(id):
            return d[2]
    return ''


def get_image(description, id):
    for d in description.values:
        if str(d[0]) == str(id):
            return d[1]
    return ''


def get_position(description, id):
    try:
        for d in description.values:
            print(d[0], str(id))
            if str(d[0]) == str(id):
                print('yes')
                return d[3], d[4]

    except Exception as e:
        print(e)
        return None, None
    return None, None


async def load_graph_handler(request):
    with_position = request.query.get('with_position', None)

    file_id = 'uploads/dataset.xlsx'
    data = pd.read_excel(file_id, header=None)
    description_data = pd.read_excel('uploads/description.xlsx')

    nodes = []
    edges = []
    formatted_nodes = []
    for i, row in enumerate(data.values):
        source = f'{str(row[0])} {str(row[1])}'
        color_value = row[2]
        target = str(row[3])

        if with_position:
            target_position_x, target_position_y = get_position(description_data, target)
        else:
            target_position_x, target_position_y = None, None
        if source not in nodes:
            nodes.append(source)
            formatted_nodes.append(
                {"id": len(nodes), "label": source, "color": calculate_color(data, color_value),
                 "font": {"color": "white"}, 'x': target_position_x, 'y': target_position_y})

        if target not in nodes:
            nodes.append(target)
            formatted_nodes.append(
                {"id": len(nodes), "label": target, "color": calculate_color(data, color_value), "x": target_position_x,
                 "y": target_position_y,
                 "font": {"color": "white"},
                 'image': get_image(description_data, target)})
        edges.append({"from": nodes.index(source) + 1, "to": nodes.index(target) + 1, "color": "white"})
    print(*formatted_nodes, sep='\n')
    return web.json_response({"nodes": formatted_nodes, "edges": edges})


app.router.add_get('/load-graph', load_graph_handler)
app.router.add_post('/storage/upload', upload_handler)
app.router.add_get('/generate', generate_handler)
app.router.add_get('/menu', menu_handler)

web.run_app(app, port=6677)
